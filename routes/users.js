var express = require('express');
var router = express.Router();
var userService = require ('../services/user-service');
var passport = require('passport');
var config = require ('../config');


router.get('/create', function(req, res, next) {
	var vm = { 
		title: "Welcome to the game"
	 };
  res.render('users/create',vm );
});



router.post('/create', function(req, res, next) {
  userService.addUser(req.body, function(err){
    if (err)
    {
      var vm ={ title: "Create an account",
                input : req.body,
                error : err
                };
      delete vm.input.password;
      return res.render("users/create",vm);
    }
    req.login(req.body,function(err){
      return res.redirect('/game');
    })
  })

});


router.post('/login',function(req,res,next)
  {
    if (req.body.rememberMe){
      req.session.cookie.maxAge = config.cookieMaxAge;
    }
    next();
  },
  passport.authenticate('local',{
  failureRedirect :'/',
  successRedirect :'/game',
  failureFlash :'Ivalid Creds'
})); 

router.get('/logout',function(req,res,next){
  req.logout();
  res.redirect('/');
});



module.exports = router;
