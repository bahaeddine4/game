var express = require('express');
var router = express.Router();
var restrict = require ('../auth/restrict');
var playerService = require ('../services/player-service');

var multer = require('multer');
var storage = multer.memoryStorage()
var upload = multer({ storage: storage });
var sharp = require('sharp');
var cv = require ('opencv');


var bigVm = {};
router.get('/',restrict, function(req, res, next) {
	var vm =  {
		title: 'The Game ',
		firstName : req.user ? req.user.firstName : null
		 }
  res.render('game/index',vm);
});



router.post('/addPlayer', upload.single('player')
, function(req, res,next) {
	 console.log ("req.file")
	 console.log (req.file)
	 console.log ("END req.file")
      	sharp(req.file.buffer)
          .resize(700, 650)
          .toFormat('jpeg')
          .toFile("public/uploads/"+req.file.originalname, function (err) {
          	if (err)
          	{
          		return next (err);
          	}
						cv.readImage("public/uploads/"+req.file.originalname, function(err, im){
							if (err){
									return next (err)
							}
						im.detectObject(cv.FACE_CASCADE, {}, function(err, faces){
							playerService.addPlayer(req.file,req.body,faces,function(err){
							if (err)
							{
								return next (err)
							}
						})
						});
					})
					res.redirect("/game#/addPlayer")
          })
  });

	router.get('/findPlayers',restrict, function(req, res, next) {
			playerService.findPlayers(function (err,players) {
				if (err)
					return next(err)
					if (players)
					res.json (players)
			});






	});


















module.exports = router;
