var mongoose = require ('mongoose');
var Schema = mongoose.Schema;


var playerSchema = new Schema ({
	impath: String,
	faces:   [Schema.Types.Mixed],
	firstChoice : String,
	secondChoice : String,
	thirdChoice : String,
	answer : String,
	created: {type: Date, default: Date.now}
});

var Player = mongoose.model('Player',playerSchema);
module.exports = {
	Player: Player
}
