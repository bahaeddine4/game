(function(){
'use strict';

angular
	.module ('app')
	.controller('playersListController',playersListController);

playersListController.$inject = ['$http','$scope'];


function playersListController ($http,$scope) {
	$scope.loading=true;
	$scope.show = false;
	$scope.checked=true;
	$scope.end=false;
	$scope.sucess=0;
	$scope.fail = 0 ;
	$http.get('/game/findPlayers').
	then (
	function(response){
			console.log("response.data");
			console.log(response.data);
			console.log("response.data");
			$scope.players = response.data;
			$scope.loading = false;
			$scope.listLength = response.data.length;
	},
	function(reason){

	})
	.catch(function(err){
		console.log (err);
	});


$scope.playersList=function(List){

	$scope.show=false;
	$scope.checked = true;
	$scope.visible = true;
	if ($scope.players.length===0)
	{
		$scope.end=true;
		return;
	}

	var maxIndex = List.length;
	var index =Math.floor((Math.random() * maxIndex));
	$scope.impath = List[index].impath;
	$scope.firstChoice = List[index].firstChoice;
	$scope.secondChoice = List[index].secondChoice;
	$scope.thirdChoice = List[index].thirdChoice;
	$scope.answer = List[index].answer;
	$scope.pathOrig = List[index].pathOrig;
	$scope.height = List[index].faces[0].height;
	$scope.width = List[index].faces[0].width;
	$scope.x = List[index].faces[0].x;
	$scope.y = List[index].faces[0].y;
	$scope.players.splice(index, 1);
	$scope.show1=false ;
	$scope.show2=false ;
	$scope.show3=false ;
	}
$scope.check = function (selectedAnswer){




		if (selectedAnswer===$scope.firstChoice)
			{
				$scope.checked = false;
				$scope.show = true;
				$scope.visible = false;
				$scope.show1=true;
		}


	else if (selectedAnswer===$scope.secondChoice)
	{
		$scope.checked = false;
		$scope.show = true;
		$scope.visible = false;
		$scope.show2=true;
	}


	else if (selectedAnswer===$scope.thirdChoice)
	{
		$scope.checked = false;
		$scope.show = true;
		$scope.visible = false;
		$scope.show3=true;
	}


	if (selectedAnswer===$scope.answer){
		console.log ("good")
		$scope.sucess ++ ;
		$scope.class1= "icon-ok-circle";
		$scope.class2="color:green ";
		return true ;
		}

		$scope.fail ++ ;
		$scope.class1= "icon-ban-circle";
		$scope.class2="color:red";
		return false ;

}
	}
}());
