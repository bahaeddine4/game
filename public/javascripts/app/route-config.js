(function(){
'use strict';

angular
	.module ('app')
	.config(config);
config.$inject = ['$routeProvider'];

function config ($routeProvider) {
	$routeProvider.
		when ('/playersList',{
			templateUrl : '/javascripts/app/play/playersList.html',
			controller : 'playersListController',
			controllerAs :'vm'
		}).
		when ('/addPlayer',{
			templateUrl :'/javascripts/app/addPlayer/addPlayer.html',
      controller : 'addPlayerController',
			controllerAs :'vm'
		});
}
}());
